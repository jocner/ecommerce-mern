const express = require('express');
const router = express.Router();
// const nuevoController = require('../controllers/nuevoContoller');
const nuevoController = require('../controllers/procesosController');
const { check } = require('express-validator');


router.post('/', 
    [
        check('tienda', 'La tienda es obligatoria').not().isEmpty(),
        check('monto', 'El monto es obligatorio').isNumeric()
    ],
    nuevoController.postNuevo
);

module.exports = router;