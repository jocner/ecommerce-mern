const mongoose = require('mongoose');

const TiendaCSchema = mongoose.Schema({
   
   email: {
    type: String,
    require: false,
    trim: true
   },
   tienda: {
        type: String,
        require: true,
        trim: true
   },
   monto: {
        type: Number,
        require: true,
        trim: false
   },
   registro: {
       type: Date,
       default: Date.now()
   }
});

module.exports = mongoose.model('TiendaC', TiendaCSchema);