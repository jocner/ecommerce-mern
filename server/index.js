const express = require('express');
const conectarDB = require('./config/db');
const cors = require('cors');

//Crear el servidor
const app = express();

//Conectar a la base de datos
conectarDB();

// habilitar cors
app.use(cors());


//Habilitar express.json
app.use(express.json({ extended: true }));

//Puerto de la app
const PORT = process.env.PORT || 4000;

//Importar rutas
 app.use('/api/agregar', require('./routes/procesos'));
 app.use('/api/descontar', require('./routes/descontar'));
 app.use('/api/consulta', require('./routes/consulta'));
 app.use('/api/nuevo', require('./routes/nuevo'));
 app.use('/api/usuario', require('./routes/usuario'));
 app.use('/api/auth', require('./routes/auth'));


//Arrancar la app
app.listen(PORT, () => {
    console.log(`El servidor esta funcionando en el puerto ${PORT}`);
});